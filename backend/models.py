"""
@Description : 数据库模型
@File        : models.py
@Project     : fastcms
@Time        : 2020/4/26 19:09
@Author      : Dexter
@Software    : PyCharm
"""
from sqlalchemy import Column, Integer, String, DateTime

from database import Base, engine


class User(Base):
    """
    用户类
    """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String)
    password = Column(String)
    last_login = Column(DateTime)
