"""
@Description : 
@File        : application.py
@Project     : fastcms
@Time        : 2020/4/26 15:06
@Author      : Dexter
@Software    : PyCharm
"""
from fastapi import FastAPI, HTTPException, Depends

from apis import users


def create_app():
    app = FastAPI()

    async def get_token_header(x_token: str):
        if x_token != 'fake-token':
            raise HTTPException(status_code=400, detail='令牌不对!')

    app.include_router(
        users.router,
        prefix='/users',
        tags=['users'],
        # dependencies=[Depends(get_token_header)]
    )

    return app
