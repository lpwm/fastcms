"""
@Description : 数据库连接
@File        : database.py
@Project     : fastcms
@Time        : 2020/4/26 19:05
@Author      : Dexter
@Software    : PyCharm
"""
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = 'sqlite:///./data.db'

engine = create_engine(SQLALCHEMY_DATABASE_URL)

DBsession = sessionmaker(bind=engine)

Base = declarative_base()
