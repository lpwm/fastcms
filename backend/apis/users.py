"""
@Description : 
@File        : users.py
@Project     : fastcms
@Time        : 2020/4/26 15:07
@Author      : Dexter
@Software    : PyCharm
"""
from fastapi import APIRouter

from database import DBsession
from models import User

router = APIRouter()
session = DBsession()


@router.get('/')
async def get_users():
    users = session.query(User).all()
    return users


@router.get('/{username}')
async def get_user(username: str):
    return {'username': username}
