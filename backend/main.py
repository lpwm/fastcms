"""
@Description : 
@File        : main.py
@Project     : fastcms
@Time        : 2020/4/26 15:20
@Author      : Dexter
@Software    : PyCharm
"""
import uvicorn

from application import create_app

if __name__ == '__main__':
    app = create_app()
    uvicorn.run(app, host='127.0.0.1', port=8000)
