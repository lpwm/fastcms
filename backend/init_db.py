"""
@Description : 
@File        : init_db.py
@Project     : fastcms
@Time        : 2020/4/26 20:03
@Author      : Dexter
@Software    : PyCharm
"""
from models import *

if __name__ == '__main__':
    Base.metadata.create_all(engine)
